/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from 'react';
import { useRequest } from '../../request';

export default function List() {
    const [loading, setLoading] = useState(true);
    const [users, setUsers] = useState([]);
    const { GET } = useRequest();

    useEffect(() => {
        const fetchUsers = async () => {
            const data = await GET("/users");
            setUsers(data);
            setLoading(false);
        }
        fetchUsers();
    }, [])

    if (loading) return <div><h1>Loading...</h1></div>

    return (
        <div className="user-root">
            <h3>User List</h3>
            <table className="user-table">
                <thead>
                    <tr className="row">
                        <th className="cell">
                            #
                        </th>
                        <th className="cell">
                            User Name
                        </th>
                        <th className="cell">
                            Email
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user, index) => (
                        <tr className="row" key={user.username}>
                            <td className="cell">
                                {index + 1}
                            </td>
                            <td className="cell">
                                {user.username}
                            </td>
                            <td className="cell">
                                {user.email}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}