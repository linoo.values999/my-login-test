import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useRequest } from '../../request';

const init = {
    username: "",
    password: "",
    confirm_password: "",
    email: "",
};

const App = () => {

    const { POST } = useRequest();
    const navigate = useNavigate();
    const [formData, setFormData] = useState(init);

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    }

    const onSubmit = async (event) => {
        event.preventDefault();
        if (!Boolean(formData.username)) {
            alert("username cannot be blank!");
            return;
        } else if (!Boolean(formData.email)) {
            alert("email cannot be blank!");
            return;
        } else if (!Boolean(formData.password)) {
            alert("password cannot be blank!");
            return;
        } else if (!Boolean(formData.password === formData.confirm_password)) {
            alert("password missed match")
            return;
        }
        delete formData.confirm_password;
        POST("/auth/register", formData)
            .then(() => {
                navigate("/auth/login");
            })
            .catch(error => {
                alert(error.message);
            })

    }


    return (
        <form onSubmit={onSubmit}>
            <input class="form-control" onChange={handleChange} placeholder="User Name" name="username" autoComplete='new-password' />
            <br />
            <input class="form-control" onChange={handleChange} placeholder="Email" type="email" name="email" autoComplete='new-password' />
            <br />
            <input class="form-control" onChange={handleChange} placeholder="Password" name="password" type="password" autoComplete='new-password' />
            <br />
            <input class="form-control" onChange={handleChange} placeholder="Confirm Password" name="confirm_password" type="password" autoComplete='new-password' />
            <br />
            <button className='btn btn-primary' type='submit'>Submit</button>
        </form >
    );
}

export default App;
