import { useContext, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { AuthContext } from '../../contexts/auth';
import { useRequest } from '../../request';

export default function Login() {
    const { POST } = useRequest();
    const { signin } = useContext(AuthContext);

    const init = {
        username: "",
        password: "",
    };

    const navigate = useNavigate();
    const [formData, setFormData] = useState(init);

    let location = useLocation();

    let from = location.state?.from?.pathname || "/";

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        })
    }

    const onSubmit = async (event) => {
        event.preventDefault();
        if (!Boolean(formData.username)) {
            alert("username cannot be blank!");
            return;
        } else if (!Boolean(formData.password)) {
            alert("password cannot be blank!");
            return;
        }

        const data = await POST("/auth/login", formData);

        localStorage.setItem("token", data.token);
        signin();
        navigate(from, { replace: true });
    }

    return (
        <form onSubmit={onSubmit}>
            <input className="form-control" placeholder='User Name' onChange={handleChange} name="username" autoComplete='new-password' />
            <br />
            <input className="form-control" placeholder="Password" onChange={handleChange} name="password" type="password" autoComplete='new-password' />
            <br />
            <button className='btn btn-primary' type='submit'>Login</button>
        </form>
    );
}