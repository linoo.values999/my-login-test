import React from 'react';
import ReactDOM from 'react-dom/client';

import reportWebVitals from './reportWebVitals';

import RequireAuth from './components/RequireAuth';
import AuthLayout from './components/Layout/AuthLayout';
import UnAuthLayout from './components/Layout/UnAuthLayout';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import User from './pages/User';

import {
	BrowserRouter,
	Routes,
	Route,
} from "react-router-dom";

import { AuthProvider } from './contexts/auth';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<React.StrictMode>
		<BrowserRouter>
			<AuthProvider>
				<Routes>
					<Route path="/auth" element={<UnAuthLayout />} >
						<Route path="/auth/login" element={<Login />} />
						<Route path="/auth/register" element={<Register />} />
					</Route>
					<Route path="/" element={
						<RequireAuth>
							<AuthLayout />
						</RequireAuth>
					} >
						<Route path="/" element={<Home />} />
						<Route path="/user" element={<User />} />
					</Route>
				</Routes>
			</AuthProvider>
		</BrowserRouter>
	</React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
