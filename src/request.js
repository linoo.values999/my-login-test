import { useContext } from "react";
import { AuthContext } from "./contexts/auth";

const host = process.env.NODE_ENV === "development" ? "http://localhost:5000" : "https://kklo-api-test.herokuapp.com";

var headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

export function useRequest() {
    const { signout } = useContext(AuthContext);

    var token = localStorage.getItem("token");

    if (token) {
        headers = {
            ...headers,
            "Authorization": `Bearer ${token}`
        }
    }

    async function manageRequest(type, url, body) {

        let options = {
            method: type,
            headers,
        }

        if (body) options.body = JSON.stringify(body);

        let response = await fetch(`${host}${url}`, options);

        return manageResponse(response);
    }

    function manageResponse(response) {
        switch (response.status) {
            case 401:
                signout();
                return;
            default:
                return response.json();
        }
    }

    async function POST(url, body) {
        return manageRequest("POST", url, body);
    }

    async function GET(url) {
        return manageRequest("GET", url);
    }

    async function PUT(url, body) {
        return manageRequest("PUT", url, body);
    }

    async function DELETE(url) {
        return manageRequest("DELETE", url);
    }

    return {
        GET,
        POST,
        PUT,
        DELETE,
    };
}