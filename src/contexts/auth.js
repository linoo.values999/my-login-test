import { useState, createContext } from "react";
import { useNavigate } from "react-router-dom";

export const AuthContext = createContext({
    auth: false,
    signin: () => { },
    signout: () => { },
});

export function AuthProvider({ children }) {
    const navigate = useNavigate();
    let [auth, setAuth] = useState(false);

    let signin = () => {
        setAuth(true);
    };

    let signout = () => {
        localStorage.clear();
        setAuth(false);
        navigate("/auth/login");
    };

    let value = { auth, signin, signout };

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}