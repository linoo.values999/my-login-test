import { Link, Outlet } from "react-router-dom";
import { useContext } from 'react';
import { AuthContext } from '../../contexts/auth';

export default function App() {

    const { signout } = useContext(AuthContext);

    function handleLogout() {
        signout();
    }

    return (
        <div className='root-layout'>
            <nav>
                <Link className='link' to="/">Home</Link>&emsp;
                <Link className='link' to="/user">User</Link>&emsp;
                <button className='link' onClick={handleLogout}>Log Out</button>
            </nav>
            <main>
                <Outlet />
            </main>
            <footer>
                This is "Footer"
            </footer>
        </div>
    );
}