import { Link, Outlet } from "react-router-dom";

export default function UnAuthLayout() {
    return (
        <div className='login-root'>
            <div className='left'>
                <h1>
                    My Login Page
                </h1>
                <p>
                    To ensure your users experience is seamless from the beginning,
                    you should design your login and register screens to be as intuitive as possible.
                    These screens are generally the first step for a user when experiencing your product.
                </p>
            </div>
            <div className='right'>
                <div>
                    <Link to="/auth/login">Sign In</Link>&emsp;|&emsp;
                    <Link to="/auth/register">Register</Link>
                </div>
                <br />
                <br />
                <Outlet />
                <footer>
                    &copy; 2022 mytest.com
                </footer>
            </div>
        </div >
    );
}