/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect } from "react";
import { Navigate, useLocation } from "react-router-dom";
import { AuthContext } from "../contexts/auth";
import { useRequest } from "../request";

export default function RequireAuth({ children }) {
    let location = useLocation();
    const { GET } = useRequest();

    const { auth, signin } = useContext(AuthContext);
    const token = localStorage.getItem("token");

    useEffect(() => {
        async function checkToken() {
            const response = await GET("/");
            if (response) {
                signin();
            }
        }

        if (token) checkToken();
    }, []);

    if (token)
        return children;

    if (!auth) {
        return <Navigate to="/auth/login" state={{ from: location }} replace />;
    }

    return children;
}